module.exports = (sequelize, DataTypes) =>{
    const Products = sequelize.define('products',{
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        product_name:{
            type:DataTypes.STRING,
            allowNull:false,
            validate:{
                notNull:true
            }
        },
        product_image:{
            type:DataTypes.STRING,
            allowNull:false,
            validate:{
                notNull:true
            }
        },
        product_id:{
            type:DataTypes.STRING,
            allowNull:false,
            validate:{
                notNull:true
            }
        },
        available:{
            type:DataTypes.BOOLEAN,
            defaultValue:false
        },
        quantity:{
            type:DataTypes.INTEGER,
            defaultValue:0
        }
       
    });

    return Products;
}