module.exports = (sequelize, DataTypes) =>{
    const Category = sequelize.define('categories',{
        id:{
            type:DataTypes.INTEGER,
            primaryKey:true,
            autoIncrement:true
        },
        category_name:{
            type:DataTypes.STRING,
            allowNull:false,
            validate:{
                notEmpty:true
            }
        },
        category_image:{
            type:DataTypes.STRING,
            allowNull:false,
            validate:{
                notEmpty:true
            }
        },
        created_at:{
            type:DataTypes.DATE,
            default:sequelize.NOW
        },
        updated_at:{
            type:DataTypes.DATE,
            default:sequelize.NOW
        },
       
    });

    return Category;
}