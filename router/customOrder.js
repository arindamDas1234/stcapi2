const express = require("express");

const customOrder = express.Router();
const passport = require("passport");
const formidable = require("formidable");
const fs = require("fs");
const types = require("detect-file-type");
const path = require("path");
const base64Image = require("base64-img");


customOrder.post('/create_custom_order', function(req,res,next){
	
	var formData = new formidable.IncomingForm();
	formData.parse(req,function(err, fields, files){
		if(err)throw err;

	var newPath = path.join(__dirname, '../custom_wallPaper')+'/'+files.image.filename;
	var oldPath = files.image.path;
		
		var rowData = fs.readFileSync(oldPath);
		let fileType = "";
		types.fromBuffer(rowData, function(err, result){
			if(err)throw err;

			fileType = result;
		});

			if(fileType.mime === "image/png" || fileType.mime == "image/jpeg"){
				let codedImage ="";
				let imageDecode = base64Image.base64(oldPath, function(err, data){
					if(err)throw err;

					base64Image.img(data, 'custom_wallPaper', fields.height.toString(), function(err, filepath) {});
				});
				fs.writeFile(newPath, rowData, function(err){
					if(err)throw err;

					console.log("file uploaded success");
				})
			}else{
				res.status(420).json({
					message:"Image type is not valid only png and jpeg file is allowed",
					status:false
				});
			}
	});
});

module.exports = customOrder;