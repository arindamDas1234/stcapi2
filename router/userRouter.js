const express = require("express");
const userRouter = express.Router();

const { body, validationResult, check } = require('express-validator');
const db = require("../models");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const key = require("../utils/key");
const passport = require("passport");
const multer = require("multer");
const fs = require("fs");
const formidable = require("formidable");
const path = require("path");


// multer({
//     limits:{fieldSize:25 *1024 *1024}
// });

const Storage = multer.diskStorage({
    destination:function(req,file,callback){
        callback(null, './images');
    },
    filename:function(req, file,callback){
        callback(null, `${file.fieldname}_${Date.now()}_${file.originalname}`);
    }
});

var upload = multer({
    Storage:Storage,
    limits:25*1024*1024
}).single('image');

// user Register

userRouter.post("/create_new_user",[check('username').notEmpty().withMessage("username is reqiured"), check("mobilenumber").notEmpty().withMessage("mobile number is required"), check("statename").notEmpty().withMessage("state is required"), check("district").notEmpty().withMessage("district field is required"), check("pincode").notEmpty().withMessage("pincode field is required"),
    check("address").notEmpty().withMessage("address field is required"),
    check("password").notEmpty().withMessage("password is required")
], async function(req,res,next){
    const errors = validationResult(req);
0
        if(!errors.isEmpty()){
            res.status(420).json({
                errors:errors.array()
            });
        }
    // check mobile number allready registered or not 
    const checkMobile = await db.Users.findOne({where:{mobilenumber:req.body.mobilenumber}});
    let passwordHash =  await bcrypt.hashSync(req.body.password, 12);

        if(checkMobile){
            res.status(420).json({
                errors:"Mobile number is allready Exists",
                status:false
            });
        }else{
           try {
            await db.Users.create({
                username:req.body.username,
                mobilenumber:req.body.mobilenumber,
                state:req.body.statename,
                district:req.body.district,
                pincode:req.body.pincode,
                address:req.body.address,
                password:passwordHash
            });
           } catch (e) {
           console.log(e);
           }

            res.status(200).json({
                message:"User create successfully",
                status:true
            });
        }
} );

userRouter.post("/login",[check("mobilenumber").notEmpty().withMessage("mobile "), check("password").notEmpty().withMessage("password is required"), check("password").isLength({min:8}).withMessage("Passwod is mini mum 8 charector")], async function(req,res,next){
    const errors = validationResult(req);

        if(!errors.isEmpty()){
            res.status(400).json({
                errors:errors.array(),
                status:false
            });
        }else{
            // check mobile number 
            const checkMobileNumber = await db.Users.findOne({where:{mobilenumber:req.body.mobilenumber}});

                if(checkMobileNumber){
                    // check password
                    const checkPassword = await bcrypt.compare(req.body.password, checkMobileNumber.password);
                    if(checkPassword){
                        const payload = {
                            id:checkMobileNumber.id,
                            name:checkMobileNumber.username
                        };
                        console.log(payload);
                       const token = jwt.sign({payload}, key.key);
                       res.status(200).json({
                           message:'User login successfully',
                           status:true,
                           token: `Bearer ${token}`
                       });
                    }else{
                        res.status(420).json({
                            errors:"Password is invalid",
                            status:false
                        });
                    }
                }else{
                    res.status(420).json({
                        errors:"Mobile number invalid",
                        status:false
                    });
                }
        }
});

userRouter.post("/editUser",passport.authenticate("jwt",{session:false}), async function(req,res,next){


let imageUrl2 = "";
let user_id = req.user.id;
    const errors = validationResult(req);

        if(! errors.isEmpty()){
            res.status(420).json({
                errors:errors.array()
            });
        }else{
           const form = new formidable.IncomingForm();
       
           form.parse(req, function(req,fields, files){
      
            var oldPath = files.image.path;
       
            let url = "";
            var newPath = path.join(__dirname,'../image')+'/'+files.image.name;
            imageUrl2 =newPath;
            const rorwFile = fs.readFileSync(oldPath);
            console.log(rorwFile);
            fs.writeFile(newPath,rorwFile, async function(error){
                if(error)throw error;
                 url = newPath;
                 try {
                     db.Users.update({mobilenumber:fields.mobilenumber,image:imageUrl2},{where:{id:user_id}});
             
                   try {
                       const udpateData =  db.Users.findOne({where:{id:user_id}});
                       res.status(200).json({
                           users:udpateData,
                           status:true,
                           message:"User updated successfully"
                       });
                       } catch (e) {
                           console.log(e);
                       }
                   
                    } catch (e) {
                    console.log(e);
                    }
                 
            });
            
           });
      
        
        
        }
});



module.exports = userRouter;