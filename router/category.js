const express = require("express");

const catRouter = express.Router();
const passport = require("passport");
const db = require("../models");

const formidable = require("formidable");
const fs = require("fs");
const path = require("path");
const fileType = require("detect-file-type");

const { body, validationResult, check } = require('express-validator');

catRouter.get("/all_category",passport.authenticate("jwt",{session:false}), async function(req,res,next){
    if(req.user){
        const categories = await db.categories.findAll();

            if(categories){
                console.log(categories);
            }
    }else{
        res.status(420).json({
            errors:"User is unauthorize",
            status:false
        });
    }
});

catRouter.get("/cat_form", function(req,res,next){
    console.log(req.url);
   res.render("cat_form",{title:"category form"});
})

catRouter.post('/create_category',passport.authenticate("jwt",{session:false}),[check("category_name").notEmpty().withMessage("Please check your categor name fiels"), check("category_image").notEmpty().withMessage("Please select an image")], async function(req,res,next){
    if(req.user){
       var formData = new formidable.IncomingForm();
      formData.parse(req,function(err, fields, files){
          
          if(fields.category_name == ""){
              res.status(420).json({
                  message:"Category name is required",
                  status:false
              });
          }else if(files.category_image.size == 0) {
            res.status(420).json({
                errors:'Image file is required'
            })
          }
          
           var oldPath = files.category_image.path;
           var newPath = path.join(__dirname, "../cat_image")+'/'+files.category_image.name;

           let rowData = fs.readFileSync(oldPath);
         fileType.fromBuffer(rowData, function(error, type){
               if(error)throw error;
               if(type.mime == 'image/jpeg' || type.mime == 'image/png'){
                   fs.writeFile(newPath, rowData, async function(error){
                       if(error)throw error;
                       db.categories.create({
                           category_name:fields.category_name,
                           category_image:newPath,
                           brandId:fields.brand_id
                       }).then(result =>{
                        if(result){
                          res.status(200).json({
                            message:"Category created successfully",
                            status:false
                          });
                        }
                       }).catch(err =>{
                        console.log(err);
                       });

                     
                   })
               }else{
                   res.status(420).json({
                       errors:"image file is not valid image/jpeg image/png are requried"
                   });
               }
           });
      
      })
    }else{
        res.status(420).json({
            errors:"Users unAuthorize",
            status:false
        });
    }
});
// edit catgory
catRouter.get("/edit_category/:id", passport.authenticate("jwt", {session:false}),[check("category_name").notEmpty().withMessage("category name is required"), check("category_image").notEmpty().withMessage("category image is required")], async function(req,res,next){
   if(req.user){
  const categories = await db.categories.findOne({where:{id:req.params.id}});
    if(categories){
        res.status(200).json({
        category:categories,
        status:true
        });
    }else{
        res.status(420).json({
            message:'Products not found',
            status:false
        });
    }
   }else{
       res.status(420).json({
           message:"User is not authorize",
           status:true
       });
   }
});

// update category
catRouter.post("/update_category/:id", passport.authenticate("jwt", {session:false}),  function(req,res,next){
    let id = req.params.id;
    if(req.user){
        var formData = new formidable.IncomingForm();
        formData.parse(req, async function(err, fields, files){
           
            if(fields.category_name ===""){
                res.status(420).json({
                    message:"category name is required",
                    status:false
                });
            }else if(files.cateogry_image.size == 0){
                await db.categories.update({
                    category_name:fields.category_name
                });

                res.status(200).json({
                    message:'Category created successfully',
                    status:true

                });
            }else{
                var newPath = path.join(__dirname,"../cat_image")+'/'+files.cateogry_image.name;
            var oldPath = files.cateogry_image.path;

                var rowData = fs.readFileSync(oldPath);
                fileType.fromBuffer(rowData,function(error, type){
                    if(error)throw error;
                    if(type.mime == "image/jpeg" || type.mime ==="image/png"){
                        fs.writeFile(newPath,rowData, async function(error){
                            if(error)throw error;

                                await db.categories.update({
                                    category_image:newPath,
                                    category_name:fields.category_name
                                },{where:{id:id}});

                                res.status(200).json({
                                    message:'Category created successfully',
                                    status:true

                                });
                        })
                    }else{
                        res.status(420).json({
                            message:"Image file is invalid png jpg is required",
                            status:false
                        });
                    }
                })
            }
            
        })
    }else{
        res.status(404).json({
            message:"User is un Authorize",
            status:false
        })
    }
} )

catRouter.get("/delete_category/:id",passport.authenticate("jwt",{session:false}),async function(req,res,next){
    let id = req.params.id;
    db.categories.destroy({where:{id:id}})
    .then(function(result){
        res.json({
            message:"Category Delete success",
            status:true
        });
    });
});

module.exports = catRouter;