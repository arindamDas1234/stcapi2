const express = require("express");
const brandRouter = express.Router();

const db = require("../models");
const passport = require("passport");
const fs = require("fs");
const path = require("path");
const formidable = require("formidable");
const types = require("detect-file-type");

brandRouter.get("/brands", function(req,res,next){
    db.brands.findAll()
    .then(result =>{
        if(result){
            res.status(420).json({
                message:"No brand is available",
                status:false
            })
        }else{
            res.status(200).json({
                brands:result,
                status:true
            });
        }
    }).catch(error =>{
      consol.log(error);
      res.status(500).json({
        errors:errors
      });
    })
});



brandRouter.post("/create_brand", passport.authenticate("jwt", {session:false}), function(req,res,next){
    console.log(req.url);
    if(req.user){
      var formData = new formidable.IncomingForm();
      formData.parse(req,function(req,fields, files){
       
          if(fields.brand_name === ""){
              res.status(420).json({
                  message:"Brand name is required",
                  status:false
              });
          }else if(files.brand_image.size == 0){
              res.status(420).json({
                  message:"Brand image is required",
                  status:false
              });
          }else{
              var oldPath = files.brand_image.path;
              var newPath = path.join(__dirname,'../brand_image')+'/'+files.brand_image.name;
               var rowData= fs.readFileSync(oldPath);
               types.fromBuffer(rowData, function(error, types){
                   if(error)throw error;

                   if(types.mime === "image/png" || types.mimen === "image/jpeg"){
                        fs.writeFile(newPath,rowData, function(error){
                            if(error)throw error;
                            console.log("file uploaded successfully");
                        });

                        db.brands.create({
                            name:fields.brand_name,
                            image:newPath
                        })
                        .then(result =>{
                            res.status(200).json({
                                message:'Brad data is uploaded successfully',
                                status:true
                            });
                        }).catch(error =>{
                          console.log(error);
                        })
                   }else{
                       res.status(420).json({
                           message:"Image file type is invalid",
                           status:false
                       });
                   }
               });
          }
      });
    }else{
        res.status(404).json({
            message:'User is not Authorize',
            status:false
        });
    }
});

brandRouter.get("/brand_edit/:id", passport.authenticate("jwt", {session:false}), function(req,res,next){
    if(req.user){
        // let  id = req.params.id;
        // var formData = new formidable.IncomingForm();
        // formData.parse(req, function(err, files, fields){
        //     var newPath = path.join(__dirname,'../brand_image')+'/'+files.brand_image.name;
        //     var oldPath = files.brand_image.path;
        //     if(err)throw err;

        //         if(fields.brand_name == ""){
                    
        //         }else if(files.brand_image.size == 0){
        //             db.brands.update({
        //                 name:fields.brand_name,
                 
        //             });

        //             res.status(200).json({
        //                 message:"Brand updated successfully",
        //                 status:true
        //             });
        //         }else{
        //             const rowData = fs.readFileSync(oldPath);
        //             types.fromBuffer(rowData, function(error,result){
        //                 if(error)throw error;
                        
        //                     if(result.mime === "image/png" && result.mime === "image/jpeg" ){
        //                         res.status(420).json({
        //                             message:"File type is invalid only png and jpeg file is supported",
        //                             status:false
        //                         });
        //                         return;
        //                     }
        //                 fs.writeFile(newPath, rowData, function(error){
        //                     if(error)throw error;

        //                     console.log("image  updated sucessfully");
        //                 });
                      
        //             });
        //             db.brands.update({
        //                 name:fields.brand_name,
        //                 image:newPath
        //             }, {where:{id:id}}).then(result =>{
        //                 res.status(200).json({
        //                     message:"Brand is updated successfully",
        //                     status:true
        //                 });
        //             })
        //         }
        // })
       db.brands.findOne({where:{id:req.params.id}})
       .then(result =>{
            if(result){
                res.status(200).json({
                    brands:result,
                    status:true
                });
            }else{
                res.status(420).json({
                    message:"Brand details is not found",
                    status:false
                })
            }
       }).then(error =>{
        console.log(error);
        res.status(500).json({
          erros:error,
          status:false
        })
       }).catch(error =>{
        console.log(error);
       });
    }else{
        res.status(420).json({
            message:"User is unauthorize",
            status:false
        });
    }
});


brandRouter.post("/update_brands/:id", passport.authenticate("jwt", {session:false}), function(req,res,next){
    if(req.user){
      var formData = new formidable.IncomingForm();
      formData.parse(req, function(err, fields, files){
        if(fields.brand_name === ""){
          res.status(420).json({
            message:"Brand name is requried",
            status:false
          })
        }else if(files.brand_image.size === 0){
          db.brands.update({
            name:fields.brand_name
          },{where:{id:req.params.id}}).then(result =>{
            res.status(200).json({
              message:" Brand  successfully",
              status:true
            });
          }).catch(error =>{
            console.log(error);
          })
        }else{
          let oldPath = files.brand_image.path;
          const rowData = fs.readFileSync(oldPath);
          
          var fileType ={};
        types.fromBuffer(rowData, function(error, type){
          if(error)throw error;

            fileType = type;
        });
        console.log(fileType.mime);
let newPath = path.join(__dirname,'../brand_image')+'/'+files.brand_image.name;
          if(fileType.mime === "image/png" || fileType.mime === "image/jpeg"){
            
              // check file allready exists or not
              fs.access(newPath, fs.F_OK, function(err){
                if(err){
                  fs.writeFile(newPath, rowData, function(err, result){
                    if(err)throw err;

                      console.log("file uploaded successfully ");
                  })
                }else{
                  console.log("file allready exists");
                }
                db.brands.update({
            name:fields.brand_name,
            image:newPath
          }, {where:{id:req.params.id}}).then( result =>{
            res.status(200).json({
              message:"Brand uploaded successfully",
              status:true
            });
          })
                
              })
          }else{
            res.status(420).json({
              message:"Image file type is invalid only png and jpeg file is supported",
              status:false
            })
          };

          
        }
      })
    }else{
      res.status(420).json({
        message:"User is not authenticate",
        status:false
      });
    }
});

brandRouter.get("/delete_brand/:id", passport.authenticate("jwt", {session:false}), function(req,res,next){
  if(req.user){ 
    let id = req.params.id;

    db.brands.destroy({
      where:{id:id}
    }).then(result =>{
      res.status(200).json({
        message:"Brand deleted successfully",
        status:true
      });
    }).catch(err =>{
      console.log(err);
    });
  }else{  
    res.status(420).json({
      message:"User is not Authorize",
      status:false
    });
  }
})

brandRouter.get("/new_brand_create", function(req,res,next){
  console.log(req.url);
  res.render("brands_form",{title:"Brand title"});
});

module.exports = brandRouter;