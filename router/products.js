const express = require("express");

const productRouter =express.Router();
const db = require("../models");
const passport = require("passport");
const fs = require("fs");
const formidable = require("formidable");
const types = require("detect-file-type");
const path = require("path");
var uuid = require("uuid-random");



productRouter.get('/getProrudtcs', function(req,res,next){
    db.products.findAll({order:[
        ['id','DESC']
    ]}).then(result =>{
        console.log(result);
    })
});

productRouter.post("/create_product", passport.authenticate("jwt",{session:false}), function(req,res,next){
   
    if(req.user){
       const formData = new formidable.IncomingForm();
       formData.parse(req,function(err,fields, files ){
         console.log(fields);
           if(fields.product_name === ""){
               res.status(420).json({
                   message:"Product name is required",
                   status:false
               });
           }else if(files.product_image.size == 0){
               res.status(420).json({
                   message:'Products image field is required',
                   status:false
               });
           }else{
               var newPath = path.join(__dirname,'../products_image')+'/'+files.product_image.name;
               var oldPath = files.product_image.path;

               const rowData = fs.readFileSync(oldPath);
               types.fromBuffer(rowData, async function(error,result){
                   if(error)throw error;

                   if(result.type ==="image/jpeg" || result.type === "image/png"){
                       res.status(420).json({
                           message:"Image file is not valid jpeg png file is requried",
                           status:false
                       });
                   }else{
                    var ids = uuid();
                       db.products.create({
                           product_name:fields.product_name,
                           product_image:newPath,
                           product_id:uuid(),
                           categoryId:fields.categoryId
                       }).then(result =>{
                        if(result){
                          res.status(200).json({
                            message:"Product create successfully",
                            status:true
                          });
                        }
                       }).catch(err =>{
                        console.log(err);
                       })

                       
                   }
               })
           }
       })
    }else{
        res.status(400).json({
            errors:"User is not authenticated",
            status:false
        });
    }
});

productRouter.get("/editProduct/:id", passport.authenticate("jwt",{session:false}), function(req,res,next){
    if(req.user){
        db.products.findOne({where:{id:req.params.id}})
        .then(result =>{
            if(result){
                res.status(200).json({
                    products:result,
                    status:true
                })
            }else{
                res.status(420).json({
                    message:'Products not found',
                    status:false
                });
            }
        }).then(error =>{
          console.log(error);
        })
    }else{
        res.status(420).json({
            message:'User is not Authorize',
            errors:false
        });
    }
});

// products update

productRouter.post('/updateProducts/:id', passport.authenticate('jwt', {session:false}), function(req,res,next){
   if(req.user){
    var formData = new formidable.IncomingForm();
    formData.parse(req, function(err, fields, files){
      if(err)throw err;

        if(fields.product_name ==="" ){
          res.status(200).json({
            message:"Product name must required",
            status:false
          });
        }else if(files.product_image.size ===0){
          db.products.update({
            product_name:fields.product_name,
            categoryId:fields.id
          }).then(result =>{
            res.status(200).json({
              message:"Product updated sucessfully",
              status:true
            });
          }).catch(err =>{
            console.log(err);
          });
        }else{
          var newPath = path.join(__dirname, '../products_image')+'/'+files.product_image.name;
          let oldPath = files.product_image.path;
          var rowData = fs.readFileSync(oldPath);

          let fileType = {};
          types.fromBuffer(rowData, function(err, content){
            if(err)throw err;

            fileType = content;
          });

          if(fileType.mime === "image/png" || fileType.mime === "image/jpeg"){
            // check if image allready exists in folder
          fs.access(newPath, fs.F_OK, function(err){
            if(err){
              fs.writeFile(newPath, rowData, function(err){
              if(err)throw err;

              console.log("file uploaded successfully");
            })
            }else{
              console.log("file exists");
              return;
            }
          });

          db.products.update({
            product_name:fields.product_name,
            product_image:newPath,
            categoryId:fields.categoryId
          }, {where:{id:req.params.id}}).then(result =>{
            res.status(200).json({
              message:"Product updated successfully",
              status:true
            });
          }).then(error =>{
            console.log(error);
          });
            
          }else{
            res.status(420).json({
              message:"Image file is invalid only png and jpeg file are supported",
              status:false
            });
          }

          
        }
    })
   }else{
    res.status(420).json({
      message:"User is not Authorize",
      status:false

    });
   }
});

productRouter.get("/delete_products/:id", passport.authenticate("{jwt",{session:false}), function(req,res,next){
    if(req.user){
        db.products.destroy({where:{id:req.params.id}})
        .then(result =>{
            res.status(200).json({
                message:'Products Deleted successfully',
                status:true
            });
        }).catch(error =>{
          console.log(error);
        });
    }else{
        res.status(420).json({
            message:'User is not Authorize',
            status:false
        });
    }
});

productRouter.get("/product_form", function(req,res,next){
  res.render("product_form",{title:"Products form"});
});

// search by pattern id

productRouter.get("/search/products/:id", function(req,res,next){
    db.products.findOne({where:{product_id:req.params.id}},{include:[{
      model:db.categories,
      as:'categories'
    }]}).then(result =>{
      if(result){
        res.status(200).json({
          product:result,
          status:true
        });
      }else{
        res.status(400).json({
          message:"No Product are found",
          status:false
        });
      }
    })
});

// search by category

productRouter.get("/product-category/:cat", function(req,res, next) {
  db.products.findOne({where:{categoryId:req.params.cat}})
  .then(result =>{
    if(result){
      res.status(200).json({
        products:result,
        status:true
      });
    }else{
      res.status(400).json({
        message:"No Products Found",
        status:false
      })
    }
  })
});

module.exports = productRouter;