const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const key = require("../utils/key.js");
const db = require("../models");

var opts = {};

opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = key.key;

module.exports = passport =>{
    passport.use(
        new JwtStrategy(opts, async function(jwt_payload,done){
            console.log(jwt_payload);
             const Users =  await db.Users.findOne({where:{id:jwt_payload.payload.id}});
             console.log(Users);

                if(Users){
                    return done(null, Users)
                }else{
                    return done(null, false);
                }
        })
    )
}