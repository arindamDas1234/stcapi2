const express = require("express");

const cors = require("./utils/cors");
const morgan = require("morgan");
const cluster = require("cluster");
const os = require("os");
const bodyParser  = require("body-parser");
const http = require("http");
require("dotenv").config();
const passport = require("passport");
const db = require("./models");
const userRouter = require("./router/userRouter.js");
const catRouter = require("./router/category.js");
const path = require("path");
const productRouter = require("./router/products");
const brandRouter = require("./router/brands");
const customOrder =require("./router/customOrder.js");


const app = express();

app.use(cors);

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:false
}));
app.set("view engine",'ejs');


app.use('/image', express.static(__dirname+'/image'));
app.use( express.static(__dirname+'/public'));
app.use("/category", express.static(__dirname+'/cat_image'));
app.use("/custom_wp", express.static(__dirname+'/custom_wallPaper'));

require("./auth/passport.js")(passport);
// user Rotuer

app.use("/api/user/",userRouter);
//cat Router 
app.use('/api/cat/', catRouter);
//products router
app.use('/api/products',productRouter);
// brand router
app.use("/api/brands",brandRouter);
app.use("/api/orders", customOrder);

// load balencer

if(cluster.isMaster){
    for(let i=0; i < os.cpus().length; i++){
        cluster.fork();
    }
    // get more number of threds from local machine
    let workerThred =[];

    for(const id in cluster.workers){
        console.log(id);
        workerThred.push(id);
    }
    // notify all cluster workers 
    workerThred.forEach(async (pid) =>{
        cluster.workers[pid].send({
            from:'isMaster',
            type:'SIGKILL',
            message:'clean up ! worker is dead'
        });
    });
    // check worker status

        if(process.env.NODE_ENV == 'production'){
            // cluster on line status
            cluster.on("online", function(worker){
                if(worker.isConnected()){
                    console.log(`Worker is connected ${worker.process.pid}`);
                }
        });

        //  cluster dead status
        cluster.on("exit", function(worker, signal, code){
            if(worker.isDead()){
                console.log(`worker is dead ${worker.process.pid}`);
            }
            cluster.fork();
        });

           cluster.on("message", function(worker,{from, message}){
               console.log(`worker ${worker.process.pid} ${from} ${message}`);
           })
        }
}else{
    if(cluster.isWorker){
        process.on("message", function(msg){
            if(msg.from == "isMaster" && msg.type =="SIGKILL"){
                process.send({
                    from:"isWorker",
                    message:`yes master ${msg.from}`
                });
            }
        });

        process.on("exit", function(){
            process.exist(process.exitCode);
        });

        var port =5000;

        const server = http.createServer(app);

        db.sequelize.sync().then(req =>{
            server.listen(port, function(){
                console.log(`server running on port ${port}`);
            })
        })

       
    }
}